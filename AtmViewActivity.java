package com.khoshra.opu.adslocator;



import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import org.mapsforge.map.android.graphics.AndroidGraphicFactory;

import java.util.ArrayList;


public class AtmViewActivity extends ActionBarActivity {


    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AndroidGraphicFactory.createInstance(this.getApplication());
        setContentView(R.layout.atm_view);



        mRecyclerView = (RecyclerView) findViewById(R.id.atm_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        GPSTracker gps = new GPSTracker(AtmViewActivity.this);
        mAdapter = new AtmRecyclerViewAdapter(getApplicationContext(), gps);


        mRecyclerView.setAdapter(mAdapter);
        RecyclerView.ItemDecoration itemDecoration =
              new DividerItemDecoration(this, LinearLayoutManager.VERTICAL);
         mRecyclerView.addItemDecoration(itemDecoration);



    }




}
