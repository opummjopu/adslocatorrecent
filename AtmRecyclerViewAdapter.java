package com.khoshra.opu.adslocator;


import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import org.mapsforge.core.graphics.Bitmap;
import org.mapsforge.core.model.LatLong;
import org.mapsforge.map.android.graphics.AndroidGraphicFactory;
import org.mapsforge.map.android.util.AndroidUtil;
import org.mapsforge.map.android.view.MapView;
import org.mapsforge.map.layer.Layers;
import org.mapsforge.map.layer.cache.TileCache;
import org.mapsforge.map.layer.overlay.Marker;
import org.mapsforge.map.layer.renderer.TileRendererLayer;
import org.mapsforge.map.rendertheme.InternalRenderTheme;

import java.io.File;
import java.util.ArrayList;

public class AtmRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<Atms> data;
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_MAP = 1;
    private static final int TYPE_SUB_ITEM = 2;
    private static final int TYPE_ITEM = 3;

    private LayoutInflater inflater;
    private Context context;

    private static DatabaseHelper dbHelper;
    private double gpslat;
    private double gpslon;
    GPSTracker gps;
    private LatLong yourLocation;


    public AtmRecyclerViewAdapter(Context context, GPSTracker gps) {
        this.context = context;
        inflater = LayoutInflater.from(context);

        dbHelper = new DatabaseHelper(context);

        this.gps = gps;
        if (gps.canGetLocation()) {
            gpslat = gps.getLatitude();
            gpslon = gps.getLongitude();
            yourLocation = new LatLong(gpslat,gpslon);
        } else {
            gps.showSettingsAlert();
        }

        double distance = 0.5;
        final ArrayList<Atms> allatms = dbHelper.getAtms(gpslat, gpslon, distance);
        this.data = allatms;


    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View view = inflater.inflate(R.layout.distance_bar, parent, false);
            HeaderHolder holder = new HeaderHolder(view);
            return holder;
        } else if(viewType == TYPE_MAP){
            View view = inflater.inflate(R.layout.your_location_map, parent, false);
            MapHolder holder = new MapHolder(view);
            return holder;
        } else if(viewType == TYPE_SUB_ITEM){
            View view = inflater.inflate(R.layout.second_menu, parent, false);
            SecondMenu holder = new SecondMenu(view);
            return holder;
        } else {
            View view = inflater.inflate(R.layout.atm_item_recyclerview, parent, false);
            ItemHolder holder = new ItemHolder(view);
            return holder;

        }

    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_HEADER;
        } else if (position == 1) {
            return TYPE_MAP;
        } else if (position == 2){
            return TYPE_SUB_ITEM;
        } else {
            return TYPE_ITEM;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof HeaderHolder) {


        }else if (holder instanceof MapHolder) {


        } else if (holder instanceof SecondMenu){

        } else {
            ItemHolder itemHolder = (ItemHolder) holder;
            Atms current = data.get(position - 3);

            String atmBankLogoID = "bank_" + current.getBankId();
            int resId = context.getResources().getIdentifier(atmBankLogoID, "drawable", context.getPackageName());
            itemHolder.atm_bank_logo.setImageResource(resId);
            itemHolder.bankNameOfAtm.setText(current.getBankName());
            itemHolder.addressOfAtm.setText(current.getAddress());
            itemHolder.distanseOfAtm.setText(String.format("%.2g km", current.getDistance()));

        }


    }

    @Override
    public int getItemCount() {
        return data.size()+3;
    }

    class ItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView atm_bank_logo;
        TextView bankNameOfAtm;
        TextView addressOfAtm;
        TextView distanseOfAtm;


        public ItemHolder(View itemView) {
            super(itemView);

            atm_bank_logo = (ImageView) itemView.findViewById(R.id.atm_bank_logo);
            bankNameOfAtm = (TextView) itemView.findViewById(R.id.bank_name_of_atm);
            addressOfAtm = (TextView) itemView.findViewById(R.id.address_of_atm);
            distanseOfAtm = (TextView) itemView.findViewById(R.id.distance_of_atm);


            itemView.setClickable(true);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            int position = getPosition();
            Atms bank = data.get(position - 3);

            final Intent mapIntent = new Intent(context, MapActivity.class);

            Bundle b = new Bundle();
            b.putDouble("gpsLat", gpslat);
            b.putDouble("gpsLon", gpslon);
            b.putDouble("atmLat", bank.getLat());
            b.putDouble("atmLon", bank.getLon());
            mapIntent.putExtras(b);

            mapIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(mapIntent);
        }
    }

    class MapHolder extends  RecyclerView.ViewHolder{
        MapView mapView;
        TileCache tileCache;
        TileRendererLayer tileRendererLayer;


        String currentArea = "bangladesh";
        File mapsFolder;

        public MapHolder(View mapview) {
            super(mapview);
            mapView = (MapView) mapview.findViewById(R.id.your_loc_map);


            mapView.setClickable(true);
            mapView.getMapScaleBar().setVisible(true);
            mapView.setBuiltInZoomControls(true);
            mapView.getMapZoomControls().setZoomLevelMin((byte) 10);
            mapView.getMapZoomControls().setZoomLevelMax((byte) 20);


            // create a tile cache of suitable size
            tileCache = AndroidUtil.createTileCache(context, "mapcache",
                    mapView.getModel().displayModel.getTileSize(), 1f,
                    mapView.getModel().frameBufferModel.getOverdrawFactor());


            mapView.getModel().mapViewPosition.setCenter(yourLocation);
            mapView.getModel().mapViewPosition.setZoomLevel((byte) 14);

            // tile renderer layer using internal render theme
            mapsFolder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
                    "/graphhopper/maps/");
            final File areaFolder = new File(mapsFolder, currentArea + "-gh");

            File mapFile = new File(areaFolder, currentArea + ".map");

            tileRendererLayer = new TileRendererLayer(tileCache,
                    mapView.getModel().mapViewPosition, false, true, AndroidGraphicFactory.INSTANCE);

            tileRendererLayer.setMapFile(mapFile);
            tileRendererLayer.setXmlRenderTheme(InternalRenderTheme.OSMARENDER);

            // only once a layer is associated with a mapView the rendering starts
            mapView.getLayerManager().getLayers().add(tileRendererLayer);

            Layers layers = mapView.getLayerManager().getLayers();
            Marker marker = createMarker(yourLocation, R.drawable.pin_purple);
            if ( marker != null)
            {
                layers.add(marker);
            }
        }
    }


    class HeaderHolder extends RecyclerView.ViewHolder implements SeekBar.OnSeekBarChangeListener {
        DottedSeekBar distanceSeekBar;

        public HeaderHolder(View headerView) {
            super(headerView);
            distanceSeekBar = (DottedSeekBar) headerView.findViewById(R.id.distance_seekBar);
            distanceSeekBar.setDots(new int[] {0,1,2, 3,4, 5,6});
            distanceSeekBar.setDotsDrawable(R.drawable.dot);
            distanceSeekBar.setOnSeekBarChangeListener(this);
        }


        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            final ArrayList<Atms> allatms_1 = dbHelper.getAtms(gpslat, gpslon, progress*0.5);
            data = allatms_1;
            notifyDataSetChanged();


        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }

    }

    class SecondMenu extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView prefBanks;
        ImageView editPrefBanks;


        public SecondMenu(View menuView) {
            super(menuView);
            prefBanks = (TextView) menuView.findViewById(R.id.pref_bank_title);
            editPrefBanks = (ImageView) menuView.findViewById(R.id.edit_pref_banks);

            editPrefBanks.setClickable(true);
            editPrefBanks.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            final Intent atmNewIntent = new Intent(context, RecyclerViewActivity.class);

           atmNewIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(atmNewIntent);
        }
    }




    private Marker createMarker( LatLong p, int resource )
    {
        Drawable drawable = context.getResources().getDrawable(resource);
        Bitmap bitmap = AndroidGraphicFactory.convertToBitmap(drawable);
        return new Marker(p, bitmap, 0, -bitmap.getHeight() / 2);
    }


}