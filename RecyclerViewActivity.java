package com.khoshra.opu.adslocator;



import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.internal.view.menu.MenuView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.SearchView;
import android.widget.Toast;

import java.sql.SQLException;
import java.util.ArrayList;

import static com.khoshra.opu.adslocator.BankRecyclerViewAdapter.*;


public class RecyclerViewActivity extends ActionBarActivity implements SearchView.OnQueryTextListener,
        SearchView.OnCloseListener {




    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private DatabaseHelper dbHelper;
    private Toolbar toolbar;
    static FloatingActionButton fab;
    private SearchView searchView;
    private PopupMenu popupMenu;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);


        toolbar = (Toolbar) findViewById(R.id.atm_appbar_one);
        setSupportActionBar(toolbar);
        setTitle(null);

        //searchView = (SearchView) findViewById(R.id.search);
        //searchView.setIconifiedByDefault(false);
       //searchView.setOnQueryTextListener(this);
        //searchView.setOnCloseListener(this);

        dbHelper = new DatabaseHelper(getApplicationContext());
        final ArrayList<Atms> allbanks = dbHelper.getAllBanks();

        final ArrayList<String> selectedBank = dbHelper.getAllSelectedBank();

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new BankRecyclerViewAdapter(getApplicationContext(),allbanks, selectedBank);


        mRecyclerView.setAdapter(mAdapter);






        fab = (FloatingActionButton) findViewById(R.id.fab);
        final int selectCount = selectedBank.size();
        showData(getApplicationContext(), selectCount + "");


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(RecyclerViewActivity.this, "Clicked pink Floating Action Button", Toast.LENGTH_SHORT).show();
                popupMenu = new PopupMenu(RecyclerViewActivity.this, v);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu menu) {
                        Toast.makeText(getApplicationContext(), "Popup Menu is dismissed",
                                Toast.LENGTH_SHORT).show();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub
                       // switch (item.getItemId()) {
                         //   case R.id.header_popup:
                          //      Toast.makeText(getApplicationContext(), "Bank name",
                           //             Toast.LENGTH_SHORT).show();
                           //     return true;

                       // }

                       // return false;
                        return false;
                    }
                });

                final ArrayList<String> selectedBank = dbHelper.getAllSelectedBank();

                popupMenu.inflate(R.menu.popup_menu);

                for (int i = 0; i < selectedBank.size(); i++) {
                    popupMenu.getMenu().add(dbHelper.getBankName(Integer.parseInt(selectedBank.get(i))));
                }
                popupMenu.show();


            }
        });



/*
        RecyclerView.ItemDecoration itemDecoration =
                new DividerItemDecoration(this, LinearLayoutManager.VERTICAL);
        mRecyclerView.addItemDecoration(itemDecoration);
*/

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);


        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        searchView.setIconified(true);
        searchView.setOnQueryTextListener(this);
        searchView.setOnCloseListener(this);

        //inflater.inflate(R.menu.menu_main, menu);
        //searchView = (SearchView) menu.findItem(R.id.action_search);
        //searchView.setVisible(true);
        //searchView = (SearchView) MenuItemCompat.getActionView(searchView);
        //mSearchView.setQueryHint(getString(R.string.search_view_hint));
        //setOnQueryTextListener(this);

       // searchView = (SearchView) menu.findItem(R.id.action_search);
        //menuItem.setOnQueryTextListener(this);
        //SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        //searchView.setIconifiedByDefault(false);
        //search.setOnQueryTextListener();
        //setOnCloseListener(this);

       // searchView = (SearchView) findViewById(R.id.action_search);
        //searchView.setIconifiedByDefault(false);
        //MenuItem.onQueryTextChange(this);
        //searchView.setOnCloseListener(this);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_done) {

            final ArrayList<Atms> allbankStatus = dbHelper.getAllBanks();

            boolean status = false;
            for (int i =0; i< allbankStatus.size(); i++){
                status = allbankStatus.get(i).getBankStatus();
                if(status){
                    break;
                }
            }

            if(status) {

                Intent atmNewIntent;
                atmNewIntent = new Intent(this,
                        AtmViewActivity.class);
                startActivity(atmNewIntent);

            }
            else {
                Toast.makeText(this, "Please select one bank", Toast.LENGTH_LONG).show();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static void showData(Context context, String number) {
        Paint paint = new Paint();
        Bitmap bkg = null;

        bkg = Bitmap.createBitmap(72, 72, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bkg);

        paint.setColor(Color.WHITE);
        paint.setTextSize(72);
        canvas.drawText(number, 16, 60, paint);
        BitmapDrawable testimg = new BitmapDrawable(context.getResources(), bkg);

        fab.setImageDrawable(testimg);
    }

    public boolean onClose() {
        try {
            showResults("");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }


    public boolean onQueryTextSubmit(String query) {
        try {
            showResults(query + "*");

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }


    public boolean onQueryTextChange(String newText) {
        //Toast.makeText(this, "Searching", Toast.LENGTH_LONG).show();
        try {
            showResults(newText + "*");
       } catch (SQLException e) {
           e.printStackTrace();
       }
        return false;
    }

    private void showResults(String query) throws SQLException {

        final Cursor cursor = dbHelper.searchBanks(query != null ? query.toString() : "@@@@");

        if(cursor == null){
            //Toast.makeText(this, "Searching", Toast.LENGTH_LONG).show();
        }
        else{

            Toast.makeText(this, "Searching", Toast.LENGTH_LONG).show();
            //mRecyclerView.setAdapter(mAdapter);
        }
    }


}

