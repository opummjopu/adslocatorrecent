package com.khoshra.opu.adslocator;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.ArrayList;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "atms_database";
    private static final String TAG = "DatabaseHelper";
    public static String DB_PATH;
    private SQLiteDatabase database;
    private Context context;

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, 1);
        this.context = context;

        String packageName = context.getPackageName();
        DB_PATH = "/data/data/" + packageName + "/databases/";
        this.database = openDatabase();

    }

    public SQLiteDatabase getDatabase(){
        String path = DB_PATH + DB_NAME;
        database = SQLiteDatabase.openDatabase(path, null,
                SQLiteDatabase.OPEN_READWRITE);
        return database;
    }

    public SQLiteDatabase openDatabase(){
        String path = DB_PATH + DB_NAME;
        if(database==null){
            createDatabase();
            Log.e(getClass().getName(), "Database created...");
            database = SQLiteDatabase.openDatabase(path, null,
                    SQLiteDatabase.OPEN_READWRITE);
        }

        return database;
    }

    private void createDatabase(){
        boolean dbExists = checkDB();
        if(!dbExists){
            this.getReadableDatabase();
            Log.e(getClass().getName()," Readable Database created...");
            copyDatabase();
            Log.e(getClass().getName(),"Database copyed...");
        }

    }

    private void copyDatabase(){
        try {
            InputStream dbInputStream = context.getAssets().open(DB_NAME);
            String path = DB_PATH + DB_NAME;
            OutputStream dbOutputStream = new FileOutputStream(path);
            byte[] buffer = new byte[4096];
            int readCount = 0;
            while( (readCount = dbInputStream.read(buffer)) >0){
                dbOutputStream.write(buffer, 0, readCount);
            }
            dbInputStream.close();
            dbOutputStream.close();

        } catch (IOException e){
            e.printStackTrace();
        }
    }




    private boolean checkDB(){
        String path = DB_PATH + DB_NAME;
        File file = new File(path);
        if(file.exists()){
            return true;
        }
        return false;
    }




    public synchronized void close(){
        if(this.database != null){
            this.database.close();
        }
    }


    private static double distance(double lat1, double lon1, double lat2, double lon2, char unit) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        if (unit == 'K') {
            dist = dist * 1.609344;
        } else if (unit == 'N') {
            dist = dist * 0.8684;
        }
        return (dist);
    }


  /*  This function converts decimal degrees to radians             */

    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }


  /* This function converts radians to decimal degrees             */

    private static double rad2deg(double rad) {
        return (rad * 180 / Math.PI);
    }



    public ArrayList<Atms> getAtms(double gpsLat, double gpsLon, double distanceFromUser){
        ArrayList<Atms> allatms = new ArrayList<Atms>();

        ArrayList<String> banks =  getAllSelectedBank();
        String[] whereArgs = new String[banks.size()];
        banks.toArray(whereArgs);

        String whereClause = "bankId IN (" ;
        for(int i=0; i<banks.size(); i++){
            if(i==banks.size()-1){
                whereClause = whereClause + "? )";
                continue;
            }
            whereClause = whereClause + " ?,";
        }

        SQLiteDatabase db = getDatabase();
        Cursor cursor = db.query("atms", null, whereClause, whereArgs, null, null, null);

        if(cursor != null && cursor.getCount() > 0){
            cursor.moveToFirst();

            for (int i =0; i< cursor.getCount(); i++){
                int atmsId = cursor.getInt(0); //cursor.getColumnIndex("'atmsid'")
                int bankId =  cursor.getInt(1);
                String address = cursor.getString(2); //cursor.getColumnIndex("'address'")
                String bankName = getBankName(bankId);
                double lat = cursor.getDouble(3); //cursor.getColumnIndex("'lat'")
                double lon = cursor.getDouble(4); //cursor.getColumnIndex("'long'")
                double distance = distance(gpsLat, gpsLon, lat, lon, 'K');
                if(distance <= distanceFromUser) {
                    Atms atm = new Atms(bankId, bankName, atmsId, lat, lon, distance, address);
                    allatms.add(atm);
                }
                cursor.moveToNext();
            }
        }
        cursor.close();
        db.close();
        return allatms;
    }



    public ArrayList<String> getAllSelectedBank(){

        ArrayList<String> allselectedbank = new ArrayList<String>();
        SQLiteDatabase db = getDatabase();


        Cursor cursor = db.query("bank",null,null,null,null,null,null);
        if(cursor != null && cursor.getCount() > 0){
            cursor.moveToFirst();

            for (int i =0; i< cursor.getCount(); i++){
                int bankId =  cursor.getInt(0);
                int bankstatus = cursor.getInt(cursor.getColumnIndex("selected"));
                if(bankstatus == 1){
                    allselectedbank.add(bankId+"");
                }


                cursor.moveToNext();
            }
        }

        cursor.close();
        db.close();
        return allselectedbank;
    }




    public ArrayList<Atms> getAllBanks(){

        ArrayList<Atms> allbanks = new ArrayList<Atms>();
        SQLiteDatabase db = getDatabase();


        Cursor cursor = db.query("bank",null,null,null,null,null,null);
        if(cursor != null && cursor.getCount() > 0){
            cursor.moveToFirst();

            for (int i =0; i< cursor.getCount(); i++){
                boolean bankStatus = false;
                int bankId =  cursor.getInt(0);
                String bankName = cursor.getString(1);
                int bankstatus = cursor.getInt(cursor.getColumnIndex("selected"));
                if(bankstatus == 1){
                    bankStatus = true;
                }
                Atms bank = new Atms(bankId, bankName, bankStatus);
                allbanks.add(bank);


                cursor.moveToNext();
            }
        }

        cursor.close();
        db.close();
        return allbanks;
    }


    public String getBankName(int bankId){

        String bankName = new String();
        String bankid = bankId+"";
        String whereClause = "bankId = ? ";

        String[] whereArgs = new String[]{
                bankid
        };
        SQLiteDatabase db = getDatabase();
        Cursor cursor = db.query("bank", null, whereClause, whereArgs,null,null,null);

        if(cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            bankName = cursor.getString(1);
        }

        db.close();
        cursor.close();

        return bankName;
    }



    public void updateStatus(int bankId){
        SQLiteDatabase db = getDatabase();

        String BankId = ""+bankId;
        String whereClause = "bankId = ? ";
        String[] whereArgs = new String[] {
                BankId
        };

        String strFilter = "bankId = " + bankId;

        Cursor cursor = db.query("bank", null, whereClause, whereArgs,null,null,null);


        if(cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();


            ContentValues value = new ContentValues();

            if(cursor.getInt(cursor.getColumnIndex("selected")) == 0 ){
                value.put("selected",1);
                db.update("bank", value, strFilter, null);
            }

            if(cursor.getInt(cursor.getColumnIndex("selected")) == 1 ) {

                value.put("selected",0);
                db.update("bank", value, strFilter, null);
            }

        }

        db.close();
        cursor.close();

    }

    public boolean checkStatus(int bankId){

        SQLiteDatabase db = getDatabase();

        String BankId = ""+bankId;
        String whereClause = "bankId = ? ";
        String[] whereArgs = new String[] {
                BankId
        };

        Cursor cursor = db.query("bank", null, whereClause, whereArgs,null,null,null);


        if(cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            int valueOfSelected = cursor.getInt(cursor.getColumnIndex("selected"));

            db.close();
            cursor.close();

            if(valueOfSelected == 1){
                return true;
            }


        }

        return false;
    }

    public Cursor searchBanks(String inputText) throws SQLException {

        SQLiteDatabase db = getDatabase();

        Log.w(TAG, inputText);

        String bankName = ""+inputText;
        String whereClause = "bankName = ? ";
        String[] whereArgs = new String[] {
                bankName
        };

        Cursor cursor = db.query("bank", null, whereClause, whereArgs,null,null,null);


        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;



    }




    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
