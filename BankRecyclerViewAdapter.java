package com.khoshra.opu.adslocator;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import java.sql.SQLException;
import java.util.ArrayList;



public class BankRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    ArrayList<Atms> data;
    private static final int TYPE_HEADER=0;
    private static final int TYPE_ITEM=1;
    private LayoutInflater inflater;
    private Context context;
    int selectCount;
    ArrayList<String> selectedbank = new ArrayList<String>();

    private static DatabaseHelper dbHelper;
    private android.view.ActionMode mActionMode;


    public BankRecyclerViewAdapter(Context context, ArrayList<Atms> data, ArrayList<String> selectedbank){
        this.context=context;
        inflater = LayoutInflater.from(context);
        this.data = data;

        this.selectedbank = selectedbank;
        if( selectedbank != null) {
            selectCount = selectedbank.size();
        } else {
            selectCount = 0;
        }

    }




    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view=inflater.inflate(R.layout.recyclerview_item, parent,false);
        ItemHolder holder=new ItemHolder(view);
        return holder;

    }

    @Override
    public int getItemViewType(int position) {

        return TYPE_ITEM;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof HeaderHolder ){


        }
        else{
            ItemHolder itemHolder = (ItemHolder) holder;
            Atms current = data.get(position);
            itemHolder.bankName.setText(current.getBankName());

            boolean status = current.getBankStatus();

            if(status) {
                itemHolder.tickMark.setVisibility(itemHolder.itemView.VISIBLE);
            }
            else {
                itemHolder.tickMark.setVisibility(itemHolder.itemView.INVISIBLE);
            }


        }

    }
    @Override
    public int getItemCount() {
        return data.size()  ;
    }


    class ItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        TextView bankName;
        ImageView tickMark;
        public ItemHolder(View itemView) {
            super(itemView);
            bankName = (TextView) itemView.findViewById(R.id.textView);
            tickMark = (ImageView) itemView.findViewById(R.id.tickMark);
            itemView.setClickable(true);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            int position = getPosition();
            Atms bank = data.get(position);
            int bankid = bank.getBankId();

            dbHelper = new DatabaseHelper(context);
            dbHelper.updateStatus(bankid);
            boolean status = dbHelper.checkStatus(bankid);




            if(status) {
                tickMark.setVisibility(itemView.VISIBLE);
                selectCount++;
                RecyclerViewActivity.showData(context, selectCount+"");

            }
            else {
                tickMark.setVisibility(itemView.INVISIBLE);
                selectCount--;
                RecyclerViewActivity.showData(context, selectCount + "");
            }

           // if(mActionMode != null){

            //}else{
             //   mActionMode = ((RecyclerViewActivity)context).startActionMode((android.view.ActionMode.Callback) new ActionBarCallBack());
                 //RecyclerViewActivity.startActionMode((android.view.ActionMode.Callback) new ActionBarCallBack());
            //}
        }


    }


    class HeaderHolder extends RecyclerView.ViewHolder {

        public HeaderHolder(View itemView) {
            super(itemView);
        }
    }


    class ActionBarCallBack implements ActionMode.Callback {

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.context_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {

        }
    }




    }